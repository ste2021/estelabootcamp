## API Login para o Likedin Bootcamp ZUP

### DEV : [Estela de Oliveira](https://github.com/ste2021)

## Objetivo do projeto

# Desenvolver uma API de login utilizando SpringBoot que serve a rede social Likedin 

### *Veja o progresso deste projeto no Trello clicando neste [link](https://trello.com/b/NQQRJYzV/zup-api)* 

```Requisitos Necessários```

- [x] Java 8 ou superior;
- [x] Apache Maven;
- [x] Uma IDE de sua preferência, aqui trabalhei com VSCode;


```Passos```

#### Crie o projeto Spring clicando *[aqui](https://start.spring.io/)
Escolha suas dependências;
Neste projeto foram utilizadas:
- [x] Spring Boot DevTools
- [x] Spring Web (MVC)
- [x] Spring Data JPA 
- [x] MySQL Driver (Configuração banco de dados)

#### Crie as classes
- [x] Controller
- [x] Models
- [x] Respository

Agora faça as ligações, defina o banco de Dados no application.properties

## Pontos a melhorar no projeto

- [x] Melhorar o código
- [x] Criar autenticações com Token

#### Observações: Ferramenta Swagger utilizada para testes


## Considerações Finais

``` Quando se trata de uma rede social tão vasta quanto o Likedin, podemos pensar em várias API's sendo consumidas por este tipo de serviço, optei pela API de login, pela produtividade e pelo curto espaço de tempo, também fiz esta escolha pelo fato de que uma tela de usuário é algo super relevante em uma rede social, e poder elaborar uma parte de uma API, mesmo que simples para um login, acredito que foi um dos primeiros passos feitos pelos criadores desta rede social, decidi utilizar o JPA pela questão da produtividade ao invés da DAO, para uma melhor organização de repositórios. Optei pelo MySQL, porque o Likedin é organizado por contatos, embora a maioria das redes sociais optem por bancos não relacionais (NoSQL), optei pela facilidade. ```
