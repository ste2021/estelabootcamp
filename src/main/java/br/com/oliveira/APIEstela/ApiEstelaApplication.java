package br.com.oliveira.APIEstela;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication

public class ApiEstelaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiEstelaApplication.class, args);
	}
}